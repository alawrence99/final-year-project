package com.example.logichelper;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.stream.Stream;

class SubformulaBuilder {

    ArrayList<String> calculateSubformulas(Graph graph, ArrayList<String> IDs){
        ArrayList<String> subformulas = new ArrayList<>();
        String subformula;
        //go through each selected node, and grow the subformula beneath it
        for (int i=0;i<IDs.size();i++){
            subformula = growFormula(graph.getNode(IDs.get(i)));
            if (subformula!=null) {
                //Only add formulas including more than just a letter
                if (subformula.length() > 3) {
                    subformulas.add(subformula);
                }
            }
        }
        return subformulas;
    }

    private static boolean checkAbove(Node selectedNode, Node nodeToCheck){
        //determine whether a node is graphically above the selected node
        Object[] selectedNodeXYZ = (Object[])selectedNode.getAttribute("xyz");
        Object[] nodeToCheckXYZ = (Object[])nodeToCheck.getAttribute("xyz");
        double selectedNodeY = (double)selectedNodeXYZ[1];
        double nodeToCheckY = (double)nodeToCheckXYZ[1];

        return nodeToCheckY > selectedNodeY;
    }

    private static HashMap<String,Node> leftOrRight(Node node1, Node node2){
        //determine which node is left, and which is right
        HashMap<String, Node> nodes = new HashMap<>();
        Object[] node1xyz = (Object[])node1.getAttribute("xyz");
        Object[] node2xyz = (Object[])node2.getAttribute("xyz");

        if ((double)node1xyz[0]<(double)node2xyz[0]){
            nodes.put("leftNode",node1);
            nodes.put("rightNode",node2);
        }else{
            nodes.put("leftNode",node2);
            nodes.put("rightNode",node1);
        }
        return nodes;
    }

    private String growFormula(Node parent){
        //recursive algorithm going down the tree joining the left and right leaves using the node operator
        Node currentNode;
        String returnString;
        HashMap<String,Node> children;

        String parentLabel = parent.getAttribute("label").toString();

        //bottom of the tree reached
        if(FormulaOperations.checkLetter(parentLabel)){
            return parentLabel;
        }
        //calculate a nodes neighbours
        Stream<? extends Node> k = parent.neighborNodes();
        ArrayList<Node> adjacentNodes = new ArrayList<>(Arrays.asList(k.toArray(Node[]::new)));
        //remove any neighbours that are above it in the tree
        for(int i=0;i<adjacentNodes.size();i++){
            currentNode = adjacentNodes.get(i);
            if (checkAbove(parent,currentNode)){
                adjacentNodes.remove(currentNode);
            }
        }
        //if the operator is a NOT, so only has one child
        if(adjacentNodes.size()==1){
            currentNode = adjacentNodes.get(0);
            returnString = "("+parentLabel+growFormula(currentNode)+")";
            return returnString;
        }
        //if the operator is different from a NOT, so has two children
        else if(adjacentNodes.size()==2){
            children = leftOrRight(adjacentNodes.get(0),adjacentNodes.get(1));
            returnString = "(" + growFormula(Objects.requireNonNull(children.get("leftNode"))) + parentLabel + growFormula(Objects.requireNonNull(children.get("rightNode"))) + ")";
            return returnString;
        }

        return null;
    }

}
