package com.example.logichelper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class EnterFormulaActivity extends AppCompatActivity {

    private EditText textBox;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_formula_screen);

        //setup keyboard
        textBox = findViewById(R.id.textBox);
        KeyboardActivity keyboard = findViewById(R.id.keyboard);
        textBox.setRawInputType(InputType.TYPE_CLASS_TEXT);
        textBox.setTextIsSelectable(false);
        InputConnection ic = textBox.onCreateInputConnection((new EditorInfo()));
        keyboard.setInputConnection(ic);

        //get buttons
        Button syntaxButton = this.findViewById(R.id.syntaxButton);
        Button cnfDnfButton = this.findViewById(R.id.cnfDnfButton);
        Button vennButton = this.findViewById(R.id.vennButton);

        //create listeners
        syntaxButtonListener syntaxButtonListener = new syntaxButtonListener();
        cnfDnfButtonListener cnfDnfButtonListener = new cnfDnfButtonListener();
        vennButtonListener vennButtonListener = new vennButtonListener();

        //register listeners to buttons
        syntaxButton.setOnClickListener(syntaxButtonListener);
        cnfDnfButton.setOnClickListener(cnfDnfButtonListener);
        vennButton.setOnClickListener(vennButtonListener);

        //setup back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        //finish the activity when the back button is pressed
        finish();
        return true;
    }


    private boolean checkFormula(String formula){
        //check the formula by seeing if any of the algorithms throw an error or unexpected result
        try {
            //generate the letters used and variable values
            ArrayList<Character> lettersUsed = FormulaOperations.calculateLettersUsed(formula);
            HashMap<Character, ArrayList<String>> variableValues = FormulaOperations.generateVariableValues(lettersUsed);

            //generate a formula that can be interpreted by jexl
            String formattedFormula = FormulaOperations.formulaFormatter(formula);
            PostfixGenerator postfixGenerator = new PostfixGenerator();
            List<String> postfixList = postfixGenerator.calculatePostfix(formattedFormula);
            String builtFormula = FormulaOperations.buildUpFormula(postfixList);

            if(builtFormula==null){
                //Display message if formula is invalid
                Toast.makeText(getApplicationContext(), "Please enter a valid statement", Toast.LENGTH_LONG).show();
                return false;
            }
            String parsedFormula = FormulaParser.parseFormula(builtFormula);
            String formulaValue = FormulaOperations.getFormulaValue(parsedFormula, 0, variableValues, lettersUsed);
            return formulaValue.equals("true") || formulaValue.equals("false");
        }
        catch(Exception e){
            //Display message if formula is invalid
            Toast.makeText(getApplicationContext(), "Please enter a valid statement", Toast.LENGTH_LONG).show();
            return false;
        }

    }


    class syntaxButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            //When button is clicked, get the intent object
            //Intent will start the syntax tree screen activity
            String formula = textBox.getText().toString();

            if(!(formula.trim().length()==0)) {
                //check formula can be parsed
                if (checkFormula(formula)) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, SyntaxTreeActivity.class);
                    intent.putExtra("formula", formula);
                    startActivity(intent);
                }
            }

        }

    }

    class cnfDnfButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            //When button is clicked, get the intent object
            //Intent will start the cnf dnf screen activity
            String formula = textBox.getText().toString();

            if(!(formula.trim().length()==0)) {
                //check formula can be parsed
                if (checkFormula(formula)) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, CnfDnfActivity.class);
                    intent.putExtra("formula", formula);
                    startActivity(intent);
                }
            }
        }
    }

    class vennButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            //When button is clicked, get the intent object
            //Intent will start the venn diagram screen activity
            String formula = textBox.getText().toString();

            if(!(formula.trim().length()==0)) {
                //check formula can be parsed
                if (checkFormula(formula)) {
                    Toast.makeText(getApplicationContext(), "Please wait", Toast.LENGTH_LONG).show();
                    Context context = v.getContext();
                    Intent intent = new Intent(context, VennDiagramActivity.class);
                    intent.putExtra("formula", formula);
                    startActivity(intent);
                }
            }
        }
    }


}
