package com.example.logichelper;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;


import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;
import org.graphstream.ui.android_viewer.util.DefaultFragment;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SyntaxTreeActivity extends AppCompatActivity implements ViewerListener {

    private static final int CONTENT_VIEW_ID = 10101010;
    private DefaultFragment fragment ;
    private Graph graph ;
    protected boolean loop = true;
    private Integer numberOfNodes;
    private String postfixString;
    private String formula;
    private ArrayList<String> includedNodesIDs = new ArrayList<>();
    private boolean started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.syntax_tree_screen);

        //retrieve the formula from the intent
        formula = getIntent().getStringExtra("formula");

        //get button
        Button truthTableButton = this.findViewById(R.id.truthTableButton);

        //create listener
        truthTableButtonListener truthTableButtonListener = new truthTableButtonListener();

        //register listener to button
        truthTableButton.setOnClickListener(truthTableButtonListener);

        //setup the back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //Create formula in postfix notation
        PostfixGenerator postfixGenerator = new PostfixGenerator();
        String formattedFormula = FormulaOperations.formulaFormatter(formula);
        List<String> postfixList = postfixGenerator.calculatePostfix(formattedFormula);
        StringBuilder sb = new StringBuilder();
        if (postfixList != null) {
            postfixList.forEach((token) -> sb.append(token).append(" "));
            postfixString = sb.toString();
            numberOfNodes = postfixList.size();


            FrameLayout frame = findViewById(R.id.treeFrame);
            frame.setId(CONTENT_VIEW_ID);

            //create the graph
            graph = new MultiGraph("syntaxTree");
            graph.setAttribute("ui.antialias");
            display(savedInstanceState, graph, false);
        }
    }


    public void shapeGraph(List<Node> nodeList){
        //layout the graph so it looks like a tree
        Node node;
        Object[] nodeXYZ;
        int j=0;
        for(int i=0;i<nodeList.size();i++){
            node = nodeList.get(i);
            nodeXYZ = (Object[]) node.getAttribute("xyz");
            if(FormulaOperations.checkNegation(node.getAttribute("label").toString())){
                j++;
                nodeList.get(j).setAttribute("xyz", nodeXYZ[0], (double)nodeXYZ[1]-1, 0.0);
            }else if(!FormulaOperations.checkLetter(node.getAttribute("label").toString())){
                j++;
                nodeList.get(j).setAttribute("xyz",(double)nodeXYZ[0]+((double)nodeXYZ[1]/2), (double)nodeXYZ[1]-1, 0.0);
                j++;
                nodeList.get(j).setAttribute("xyz",(double)nodeXYZ[0]-((double)nodeXYZ[1]/2), (double)nodeXYZ[1]-1, 0.0);
            }
        }
    }


    public void explore(Node source) {
        //iterate through the graph breadth first
        source.setAttribute("ui.class", "included");
        includedNodesIDs.add(source.getId());
        Iterator<? extends Node> k = source.getBreadthFirstIterator();
        List<Node> nodeList = new ArrayList<>();
        while (k.hasNext()) {
            Node next = k.next();
            nodeList.add(next);
            if(FormulaOperations.checkLetter(next.getAttribute("label").toString())){
                next.setAttribute("ui.class", "included");
            }
            sleep(10);
        }
        shapeGraph(nodeList);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!started) {
            started=true;

            ViewerPipe pipe = fragment.getViewer().newViewerPipe();
            pipe.addAttributeSink(graph);
            pipe.addViewerListener(this);
            pipe.pump();

            fragment.getViewer().setCloseFramePolicy(Viewer.CloseFramePolicy.CLOSE_VIEWER);

            graph.setAttribute("ui.stylesheet", styleSheet);
            graph.setAttribute("ui.antialias");

            //Tree drawing algorithm
            List<Node> stack = new ArrayList<>();
            List<Node> queue = new ArrayList<>();
            Node queueItem;
            Node node1;
            Node node2;
            String edgeID;
            String queueItemID;
            String node1ID;
            String node2ID;


            //first create all the nodes
            for (int i = 0; i < numberOfNodes; i++) {
                Node tempNode = graph.addNode(Integer.toString(i));
                tempNode.setAttribute("xyz", 0.0, 3.0, 0.0);
                tempNode.setAttribute("label", postfixString.substring(2 * i, 2 * i + 1));
                tempNode.setAttribute("ui.class", "excluded");
                queue.add(tempNode);
            }

            while (queue.size() > 0) {
                queueItem = queue.get(0);
                queueItemID = queueItem.getId();
                if (FormulaOperations.checkLetter(queueItem.getAttribute("label").toString())) {
                    stack.add(queueItem);
                    queue.remove(0);
                } else {
                    //create edges
                    node1 = stack.get(stack.size() - 1);
                    node1ID = node1.getId();
                    if (FormulaOperations.checkNegation((queueItem.getAttribute("label").toString()))) {
                        edgeID = queueItemID + node1ID;
                        graph.addEdge(edgeID, queueItemID, node1ID, false);
                        stack.remove(stack.size() - 1);
                        stack.add(queueItem);
                        queue.remove(0);
                    } else {
                        node2 = stack.get(stack.size() - 2);
                        node2ID = node2.getId();

                        edgeID = queueItemID + node1ID;
                        graph.addEdge(edgeID, queueItemID, node1ID, false);

                        edgeID = queueItemID + node2ID;
                        graph.addEdge(edgeID, queueItemID, node2ID, false);

                        stack.remove(stack.size() - 1);
                        stack.remove(stack.size() - 1);
                        stack.add(queueItem);
                        queue.remove(0);
                    }

                }


            }


            new Thread(() -> {
                while (loop) {
                    pipe.pump();
                    sleep(100);
                }

            }).start();


            explore(graph.getNode(numberOfNodes - 1));

        }else{
            finish();
        }
    }


    public void display(Bundle savedInstanceState, Graph graph, boolean autoLayout) {
        if (savedInstanceState == null) {
            FragmentManager fm = getFragmentManager();

            // find fragment or create one
            fragment = (DefaultFragment) fm.findFragmentByTag("fragment_tag");
            if (null == fragment) {
                fragment = new DefaultFragment();
                fragment.init(graph, autoLayout);
            }

            // Add the fragment in the layout and commit
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(CONTENT_VIEW_ID, fragment).commit();

        }
    }



    private String styleSheet = ""
            + "graph {"
            + "	canvas-color: white;  "
            + "	fill-mode: gradient-radial; "
            + "	fill-color: white, #EEEEEE; "
            + "	padding: 100px; "
            + "}"
            + ""
            + "node {"
            + " shape: rounded-box;"
            + " fill-mode: plain;"
            + " size: 80px;"
            + " size-mode: dyn-size;"
            + " fill-color: #CCCC;"
            + " stroke-mode: plain; "
            + " stroke-color: black; "
            + " stroke-width: 9px; "
            + " text-size: 50px;"
            + "} "
            + ""
            + "node.included { "
            + "	stroke-mode: plain; "
            + "	stroke-color: green; "
            + "}"
            + ""
            + "node.excluded { "
            + "	stroke-mode: plain; "
            + "	stroke-color: red; "
            + "}"
            + ""
            + "edge { 	shape: line; size: 5px; fill-color: grey; 	fill-mode: plain; 	arrow-shape: arrow; arrow-size: 20px, 15px; } ";


    public void buttonPushed(String id) {
        //change the status of a node when pressed
        Node nodeMoved = graph.getNode(id);
        String currentClass = nodeMoved.getAttribute("ui.class").toString();
        if(!FormulaOperations.checkLetter(nodeMoved.getAttribute("label").toString())) {
            if (currentClass.equals("excluded")) {
                nodeMoved.setAttribute("ui.class", "included");
                includedNodesIDs.add(nodeMoved.getId());
            }
            if (currentClass.equals("included")) {
                nodeMoved.setAttribute("ui.class", "excluded");
                includedNodesIDs.remove(nodeMoved.getId());
            }

        }
    }

    //required methods for implementing a viewer listener
    public void buttonReleased(String id) {

    }

    public void mouseOver(String id) {

    }

    public void mouseLeft(String id) {

    }

    public void viewClosed(String viewName) {
        loop = true;
    }

    protected void sleep( long ms ) {
        try {
            Thread.sleep( ms );
        } catch (InterruptedException e) { e.printStackTrace(); }
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        //stop the thread and end the activity when the back button is pressed
        loop=false;
        finish();
        return true;
    }

    class truthTableButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            //gather the subformulas
            SubformulaBuilder formulaBuilder = new SubformulaBuilder();
            ArrayList<String> subformulas = formulaBuilder.calculateSubformulas(graph, includedNodesIDs);
            //Check at least one node is selected
            if(subformulas.size()!=0) {
                //When button is clicked, get the intent object
                //Intent will start the truth table screen activity
                Context context = v.getContext();
                Intent intent = new Intent(context, TruthTableActivity.class);
                intent.putExtra("formula", formula);
                intent.putStringArrayListExtra("subformulas", subformulas);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(), "Please select at least one node to be displayed in the truth table", Toast.LENGTH_LONG).show();
            }
        }
    }


}
