package com.example.logichelper;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


class PostfixGenerator {

    //Creating variables for each of the operators
    private static final String AND = "\u22C0";
    private static final String NOT = "¬";
    private static final String OR = "\u22C1";
    private static final String IMPLIES = "\u2192";
    private static final String BI_IMPLIES = "\u2194";

    //Create the precedence map for the operators
    private static final Map<String, Integer> operatorPrecedenceMap;

    static {
        operatorPrecedenceMap = new HashMap<>();
        operatorPrecedenceMap.put(NOT, 1);
        operatorPrecedenceMap.put(AND, 2);
        operatorPrecedenceMap.put(OR, 3);
        operatorPrecedenceMap.put(IMPLIES, 4);
        operatorPrecedenceMap.put(BI_IMPLIES, 5);
        //As in boolean logic implies and bi-implication have equivalent precedence, we will just
        //say that bi-implication has lower precedence so we are consistent
    }


    //the shunting yard algorithm
    //returns either the list of tokens in postfix notation, or null is there is an invalid input
    private static List<String> implementShuntingYard(final Deque<String> tokens, final Map<String, Integer> precedenceMap) {

        final Deque<String> operatorStack = new LinkedList<>();
        final Deque<String> outputQueue = new LinkedList<>();
        String previousToken = "";

        while (!tokens.isEmpty()) {
            final String currentToken = tokens.removeFirst();

            if (checkVariableOrOperator(currentToken)) {
                outputQueue.add(currentToken);

            } else if (checkOperator(currentToken)) {
                while (!operatorStack.isEmpty()) {
                    if (checkOperator(operatorStack.getLast()) && precedenceCompare(operatorStack.getLast(), currentToken, precedenceMap)) {
                        outputQueue.addLast(operatorStack.removeLast());
                    } else {
                        break;
                    }
                }

                operatorStack.addLast(currentToken);
            } else if (currentToken.equals("(")) {
                if (checkVariableOrOperator(previousToken)) {
                    return null;
                }

                operatorStack.addLast("(");
            } else if (currentToken.equals(")")) {
                if (!checkVariableOrOperator(previousToken)) {
                    return null;
                }

                while (!operatorStack.isEmpty() && !operatorStack.getLast().equals("(")) {
                    outputQueue.addLast(operatorStack.removeLast());
                }

                if (operatorStack.isEmpty()) {
                    // Brackets are invalid
                    return null;
                } else {
                    // Remove left bracket
                    operatorStack.removeLast();
                }
            } else {
                //should never be called as there is a custom keyboard
                throw new IllegalStateException("Could not recognise a token: " + currentToken);
            }

            previousToken = currentToken;
        }

        while (!operatorStack.isEmpty()) {
            final String operator = operatorStack.removeLast();

            // Brackets are not equal
            if (operator.equals("(") || operator.equals(")")) {
                return null;
            }

            outputQueue.addLast(operator);
        }

        return new ArrayList<>(outputQueue);
    }



    //compares the precedence of the token with the token at the top of the stack
    private static boolean precedenceCompare(final String stackTopToken, final String token, final Map<String, Integer> precedenceMap) {
        return Objects.requireNonNull(precedenceMap.get(stackTopToken)) < Objects.requireNonNull(precedenceMap.get(token));
    }



    //checks if token is a variable or an operator
    private static boolean checkVariableOrOperator(final String token) {
        if (checkOperator(token)) {
            return false;
        }

        if (token.equals("(")) {
            return false;
        }

        if (token.equals(")")) {
            return false;
        }

        return !token.isEmpty();
    }

    //check whether the token is an operator
    private static boolean checkOperator(final String token) {
        switch (token) {
            case AND:
            case NOT:
            case OR:
            case IMPLIES:
            case BI_IMPLIES:
                return true;

            default:
                return false;
        }
    }


    //splits the formula into a list of tokens
    private static Deque<String> toTokenList(final String text) {
        final Deque<String> tokenList = new ArrayDeque<>();

        int index = 0;

        while (index < text.length()) {
            if (text.substring(index).startsWith(AND)) {
                index += AND.length();
                tokenList.add(AND);
            } else if (text.substring(index).startsWith(OR)) {
                index += OR.length();
                tokenList.add(OR);
            } else if (text.substring(index).startsWith(NOT)) {
                index += NOT.length();
                tokenList.add(NOT);
            } else if (text.substring(index).startsWith(IMPLIES)) {
                index += IMPLIES.length();
                tokenList.add(IMPLIES);
            } else if (text.substring(index).startsWith(BI_IMPLIES)) {
                index += BI_IMPLIES.length();
                tokenList.add(BI_IMPLIES);
            } else if (text.charAt(index) == '(') {
            ++index;
            tokenList.add("(");
            } else if (text.charAt(index) == ')') {
                ++index;
                tokenList.add(")");
            } else {
                int index2 = index;

                while (index2 < text.length()
                        && !Character.isWhitespace(text.charAt(index2))
                        && text.charAt(index2) != '('
                        && text.charAt(index2) != ')') {
                    ++index2;
                }

                final String variableName = text.substring(index, index2);
                index += variableName.length();
                tokenList.add(variableName);
            }

            index = advancePastWhitespace(text, index);
        }

        return tokenList;
    }

    //moves past the whitespace in the formula
    private static int advancePastWhitespace(final String text, int index) {
        while (index < text.length()
                && Character.isWhitespace(text.charAt(index))) {
            ++index;
        }

        return index;
    }



    //main method
    List<String> calculatePostfix(String formula) {

        final List<String> postfixTokenList = implementShuntingYard(toTokenList(formula),operatorPrecedenceMap);

        if (postfixTokenList == null) {
            System.out.println("Invalid token sequence");
            return null;
        } else {

            return postfixTokenList;
        }
    }
}