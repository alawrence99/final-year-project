package com.example.logichelper;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

class FormulaOperations {

    static String buildUpFormula(List<String> queue){
        //build up the formula from postfix notation
        List<String> stack = new ArrayList<>();
        String queueItem;
        String item1;
        String item2;
        String stringToAdd;

        //use the second part of the shunting yard algorithm
        //we use a similar algorithm when building the syntax tree
        while (queue.size() > 0) {
            queueItem = queue.get(0);
            if (checkLetter(queueItem)){
                stack.add(queueItem);
                queue.remove(0);
            } else {
                //create subformula
                item1 = stack.get(stack.size() - 1);
                if (checkNegation(queueItem)) {
                    stringToAdd = "("+queueItem+item1+")";
                    stack.remove(stack.size() - 1);
                    stack.add(stringToAdd);
                    queue.remove(0);
                } else {
                    item2 = stack.get(stack.size() - 2);
                    stringToAdd = "(" + item2 + queueItem + item1 + ")";
                    stack.remove(stack.size() - 1);
                    stack.remove(stack.size() - 1);
                    stack.add(stringToAdd);
                    queue.remove(0);
                }
            }
        }
        if(stack.size()==1){
            return stack.get(0);
        }
        return null;
    }

    static String getFormulaValue(String formula, int row, HashMap<Character, ArrayList<String>> variableValues, ArrayList<Character> lettersUsed){
        //calculate the truth value for the given subformula and row in the table
        JexlEngine jexl = new JexlEngine();
        jexl.setSilent(true);
        jexl.setLenient(true);

        Expression expression = jexl.createExpression(formula);
        JexlContext jexlContext = new MapContext();
        String letterValue;
        //assign truth values to each variable
        for (int i=0; i<lettersUsed.size();i++){
            letterValue = Objects.requireNonNull(variableValues.get(lettersUsed.get(i))).get(row);
            if(letterValue.equals("1")){
                jexlContext.set(lettersUsed.get(i).toString(),true);
            }else{
                jexlContext.set(lettersUsed.get(i).toString(),false);
            }
        }
        //evaluate the formula
        return String.valueOf(expression.evaluate(jexlContext));
    }

    static boolean checkLetter(String letterToCheck){
        char letter = letterToCheck.charAt(0);
        return (int) letter < 69 && (int) letter > 64;
    }

    static boolean checkNegation(String letterToCheck){
        char letter = letterToCheck.charAt(0);
        return letter == '¬';
    }

    static HashMap<Character, ArrayList<String>> generateVariableValues(ArrayList<Character> lettersUsed){
        //generate the values of the variables
        HashMap<Character, ArrayList<String>> variableValues = new HashMap<>();
        int amountOfRows= (int)Math.pow(2,lettersUsed.size());
        int item;

        for (int column=0;column<lettersUsed.size();column++){
            ArrayList<String> columnValues = new ArrayList<>();
            for (int row=0;row<amountOfRows;row++){
                item = (row/(int) Math.pow(2, lettersUsed.size()-(column+1)))%2;
                columnValues.add(String.valueOf(item));
            }
            variableValues.put(lettersUsed.get(column),columnValues);
        }
        return variableValues;
    }

    static ArrayList<Character> calculateLettersUsed(String formula){
        ArrayList<Character> lettersUsed = new ArrayList<>();

        for (int i=0;i<formula.length();i++){
            Character c = formula.charAt(i);
            if((int)c>64 && (int)c<69){
                if(!lettersUsed.contains(c)){
                    lettersUsed.add(c);
                }
            }
        }
        Collections.sort(lettersUsed);

        return lettersUsed;
    }

    static String formulaFormatter(String formulaToFormat){
        //format the formula so it is separated by spaces for the logic handler
        List<Character> chars = new ArrayList<>();
        for (char ch : formulaToFormat.toCharArray()) {
            chars.add(ch);
            chars.add(' ');
        }
        chars.remove(chars.size()-1);

        String formulaString;
        StringBuilder sb = new StringBuilder();
        chars.forEach((token) -> sb.append(token).append(" "));
        formulaString = sb.toString();

        return formulaString;
    }

}
