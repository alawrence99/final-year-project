package com.example.logichelper;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;


public class VennDiagramActivity extends AppCompatActivity{

    LinearLayout screenLayout;
    String formula;
    TextView formulaTextBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.venn_diagram_screen);

        //retrieve the formula from the intent and insert it into a text view
        formula = getIntent().getStringExtra("formula");
        formulaTextBox = findViewById(R.id.vennDiagramFormula);
        formulaTextBox.setText(formula);

        //setup back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //draw the venn diagram and add it to the screen
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.TOP|Gravity.CENTER_HORIZONTAL;
        Context context = getApplicationContext();
        screenLayout = findViewById(R.id.vennDiagramScreenLayout);
        View vennDiagramView = new VennDiagramDrawer(context,formula);
        screenLayout.addView(vennDiagramView,params);
    }



    public boolean onOptionsItemSelected(MenuItem item){
        //finish the activity when the back button is pressed
        finish();
        return true;
    }
}
