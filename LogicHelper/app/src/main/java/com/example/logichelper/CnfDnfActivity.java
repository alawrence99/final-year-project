package com.example.logichelper;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class CnfDnfActivity extends AppCompatActivity {

    TextView formulaTextBox;
    TextView cnfTextBox;
    TextView dnfTextBox;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cnf_dnf_screen);

        //setup back button
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //setup text boxes
        formulaTextBox = findViewById(R.id.originalFormulaTextBox);
        cnfTextBox = findViewById(R.id.cnfResultTextBox);
        dnfTextBox = findViewById(R.id.dnfResultTextBox);

        String formula = getIntent().getStringExtra("formula");
        formulaTextBox.setText(formula);

        //calculate the letters used and variable values
        ArrayList<Character> lettersUsed = FormulaOperations.calculateLettersUsed(formula);
        HashMap<Character,ArrayList<String>> variableValues = FormulaOperations.generateVariableValues(lettersUsed);

        //generate a formula that can be interpreted by jexl
        String formattedFormula = FormulaOperations.formulaFormatter(formula);
        PostfixGenerator postfixGenerator = new PostfixGenerator();
        List<String> postfixList = postfixGenerator.calculatePostfix(formattedFormula);
        String builtFormula = FormulaOperations.buildUpFormula(postfixList);
        String parsedFormula = FormulaParser.parseFormula(builtFormula);

        dnfTextBox.setText(convertToDnf(parsedFormula, variableValues, lettersUsed));
        cnfTextBox.setText(convertToCnf(parsedFormula, variableValues, lettersUsed));
    }

    public String convertToDnf(String formula, HashMap<Character,ArrayList<String>> variableValues, ArrayList<Character> lettersUsed){
        //use the formula truth values to construct the dnf
        String formulaValue;
        ArrayList<String> dnfSections = new ArrayList<>();
        StringBuilder stringToAdd = new StringBuilder();

        //iterate through all possible values of the variables
        for (int i=0;i<(Math.pow(2,lettersUsed.size()));i++){
            formulaValue = FormulaOperations.getFormulaValue(formula, i, variableValues, lettersUsed);
            //if the formula is true for the current variable values, build a section of the dnf
            if (formulaValue.equals("true")){
                stringToAdd.replace(0,stringToAdd.length(),"(");

                for (int j=0;j<lettersUsed.size();j++) {
                    if(Objects.requireNonNull(variableValues.get(lettersUsed.get(j))).get(i).equals("1")){
                        stringToAdd.append(lettersUsed.get(j));
                        stringToAdd.append("\u22C0");
                    }else{
                        stringToAdd.append("¬");
                        stringToAdd.append(lettersUsed.get(j));
                        stringToAdd.append("\u22C0");
                    }
                }

                stringToAdd.deleteCharAt(stringToAdd.length()-1);
                stringToAdd.append(")");
                dnfSections.add(stringToAdd.toString());
            }
        }

        StringBuilder dnfFormula = new StringBuilder();
        //Check that it is not a contradiction
        if(dnfSections.size()!=0) {
            //join the separate parts of the dnf together
            String currentString;
            for (int k = 0; k < dnfSections.size(); k++) {
                currentString = dnfSections.get(k);
                dnfFormula.append(currentString);
                dnfFormula.append("\u22C1");
            }
            dnfFormula.deleteCharAt(dnfFormula.length() - 1);
        }else{
            dnfFormula.append("False");
        }
        return dnfFormula.toString();
    }

    private String convertToCnf(String formula, HashMap<Character,ArrayList<String>> variableValues, ArrayList<Character> lettersUsed){
        //use the formula truth values to construct the cnf
        String formulaValue;
        ArrayList<String> cnfSections = new ArrayList<>();
        StringBuilder stringToAdd = new StringBuilder();

        //iterate through all possible values of the variables
        for (int i=0;i<(Math.pow(2,lettersUsed.size()));i++){
            formulaValue = FormulaOperations.getFormulaValue(formula, i, variableValues, lettersUsed);
            //if the formula is false for the current variable values, build a section of the cnf
            if (formulaValue.equals("false")){
                stringToAdd.replace(0,stringToAdd.length(),"(");

                for (int j=0;j<lettersUsed.size();j++) {
                    if(Objects.requireNonNull(variableValues.get(lettersUsed.get(j))).get(i).equals("1")){
                        stringToAdd.append("¬");
                        stringToAdd.append(lettersUsed.get(j));
                        stringToAdd.append("\u22C1");
                    }else{
                        stringToAdd.append(lettersUsed.get(j));
                        stringToAdd.append("\u22C1");
                    }
                }

                stringToAdd.deleteCharAt(stringToAdd.length()-1);
                stringToAdd.append(")");
                cnfSections.add(stringToAdd.toString());
            }
        }

        StringBuilder cnfFormula = new StringBuilder();
        //Check that it is not a tautology
        if(cnfSections.size()!=0) {
            //join the separate parts of the cnf together
            String currentString;
            for (int k = 0; k < cnfSections.size(); k++) {
                currentString = cnfSections.get(k);
                cnfFormula.append(currentString);
                cnfFormula.append("\u22C0");
            }
            cnfFormula.deleteCharAt(cnfFormula.length() - 1);
        }else{
            cnfFormula.append("True");
        }
        return cnfFormula.toString();
    }


    public boolean onOptionsItemSelected(MenuItem item){
        //finish the activity when the back button is pressed
        finish();
        return true;
    }
}
