package com.example.logichelper;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.LinearLayout;

public class KeyboardActivity extends LinearLayout implements View.OnClickListener{


    private SparseArray<String> keyValues = new SparseArray<>();
    private InputConnection inputConnection;

    public KeyboardActivity(Context context) {
        this(context, null, 0);
    }

    public KeyboardActivity(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public KeyboardActivity(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.keyboard, this, true);

        Button leftBracketButton = findViewById(R.id.leftBracketButton);
        Button rightBracketButton = findViewById(R.id.rightBracketButton);
        Button andButton = findViewById(R.id.andButton);
        Button orButton = findViewById(R.id.orButton);
        Button notButton = findViewById(R.id.notButton);
        Button impliesButton = findViewById(R.id.impliesButton);
        Button bi_impliesButton = findViewById(R.id.bi_impliesButton);
        Button letter1Button = findViewById(R.id.letter1Button);
        Button letter2Button = findViewById(R.id.letter2Button);
        Button letter3Button = findViewById(R.id.letter3Button);
        Button letter4Button = findViewById(R.id.letter4Button);
        Button deleteButton = findViewById(R.id.deleteButton);

        leftBracketButton.setOnClickListener(this);
        rightBracketButton.setOnClickListener(this);
        andButton.setOnClickListener(this);
        orButton.setOnClickListener(this);
        notButton.setOnClickListener(this);
        impliesButton.setOnClickListener(this);
        bi_impliesButton.setOnClickListener(this);
        letter1Button.setOnClickListener(this);
        letter2Button.setOnClickListener(this);
        letter3Button.setOnClickListener(this);
        letter4Button.setOnClickListener(this);
        deleteButton.setOnClickListener(this);

        keyValues.put(R.id.leftBracketButton, getResources().getString(R.string.leftBracket));
        keyValues.put(R.id.rightBracketButton, getResources().getString(R.string.rightBracket));
        keyValues.put(R.id.andButton, getResources().getString(R.string.and));
        keyValues.put(R.id.orButton, getResources().getString(R.string.or));
        keyValues.put(R.id.notButton, getResources().getString(R.string.not));
        keyValues.put(R.id.impliesButton, getResources().getString(R.string.implies));
        keyValues.put(R.id.bi_impliesButton, getResources().getString(R.string.bi_implication));
        keyValues.put(R.id.letter1Button, getResources().getString(R.string.letter1));
        keyValues.put(R.id.letter2Button, getResources().getString(R.string.letter2));
        keyValues.put(R.id.letter3Button, getResources().getString(R.string.letter3));
        keyValues.put(R.id.letter4Button, getResources().getString(R.string.letter4));
    }


    @Override
    public void onClick(View view) {
        if (inputConnection == null)
            return;

        if (view.getId() == R.id.deleteButton) {
            CharSequence selectedText = inputConnection.getSelectedText(0);

            if (TextUtils.isEmpty(selectedText)) {
                inputConnection.deleteSurroundingText(1, 0);
            } else {
                inputConnection.commitText("", 1);
            }
        } else {
            String value = keyValues.get(view.getId());
            inputConnection.commitText(value, 1);
        }
    }

    public void setInputConnection(InputConnection ic) {
        inputConnection = ic;
    }

}
