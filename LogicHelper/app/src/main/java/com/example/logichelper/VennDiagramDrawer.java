package com.example.logichelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

public class VennDiagramDrawer extends View {

    String formula;
    HashMap<Character,ArrayList<String>> variableValues;
    ArrayList<Character> lettersUsed;

    //Positions of different regions within the venn diagrams
    float[][] oneCirclePairs = {{0.1f,0.1f},{0.5f,0.5f}};
    float[][] twoCirclePairs = {{0.1f,0.1f},{0.75f,0.5f},{0.25f,0.5f},{0.5f,0.5f}};
    float[][] threeCirclePairs = {{0.1f,0.1f},{0.75f,0.75f},{0.25f,0.75f},{0.5f,0.75f},
            {0.5f,0.25f},{0.65f,0.45f},{0.35f,0.45f},{0.5f,0.5f}};
    float[][] fourCirclePairs = {{0.1f,0.1f},{0.85f,0.5f},{0.7f,0.25f},{0.75f,0.4f},
            {0.3f,0.25f},{0.7f,0.65f},{0.5f,0.35f},{0.65f,0.5f},
            {0.15f,0.5f},{0.5f,0.8f},{0.35f,0.7f},{0.42f,0.72f},
            {0.3f,0.4f},{0.58f,0.72f},{0.4f,0.5f},{0.5f,0.6f}};

    //positions of the text in the venn diagrams
    float[][] oneCircleTextPairs = {{0f,0f}};
    float[][] twoCircleTextPairs = {{-0.75f,-0.6f},{0.75f,-0.6f}};
    float[][] threeCircleTextPairs = {{-0.55f,-0.75f},{-0.75f,0.9f},{0.75f,0.9f}};
    float[][] fourCircleTextPairs = {{-0.85f,-0.5f},{-0.5f,-0.75f},{0.5f,-0.75f},{0.85f,-0.5f}};

    //required constructors for a view
    public VennDiagramDrawer(Context context) {
        super(context);
    }

    public VennDiagramDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VennDiagramDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public VennDiagramDrawer(Context context, String givenFormula){
        super(context);
        formula = givenFormula;

    }

    @Override
    @SuppressLint("DrawAllocation")
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //generate the letters used and variable values
        lettersUsed = FormulaOperations.calculateLettersUsed(formula);
        variableValues = FormulaOperations.generateVariableValues(lettersUsed);

        //generate a formula that can be interpreted by jexl
        String formattedFormula = FormulaOperations.formulaFormatter(formula);
        PostfixGenerator postfixGenerator = new PostfixGenerator();
        List<String> postfixList = postfixGenerator.calculatePostfix(formattedFormula);
        String builtFormula = FormulaOperations.buildUpFormula(postfixList);
        String parsedFormula = FormulaParser.parseFormula(builtFormula);

        //determine how many circles the venn diagram will need
        HashMap<String,Object> diagramProperties = getDiagramType(lettersUsed);

        int width = getWidth();
        int height = getHeight();
        canvas.translate(width/2f,height/2f);

        //create the venn diagram, and colour it by truth values
        Bitmap bitmapDiagram = BitmapFactory.decodeResource(getResources(),(int)diagramProperties.get("Layout"));
        Bitmap scaled = Bitmap.createScaledBitmap(bitmapDiagram, getWidth(), getWidth(), true);
        Bitmap colouredBitmap = FloodFill(scaled,(float[][]) Objects.requireNonNull(diagramProperties.get("Points")),Color.BLACK,parsedFormula);
        canvas.drawBitmap(colouredBitmap,-colouredBitmap.getWidth()/2f,-colouredBitmap.getHeight()/2f,null);

        //add the letters onto the diagram
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(100);
        paint.setTextAlign(Paint.Align.CENTER);
        int x;
        int y;
        for(int i=0;i<lettersUsed.size();i++) {
            x = (int) (((float[][]) Objects.requireNonNull(diagramProperties.get("Text")))[i][0] *(colouredBitmap.getWidth()/2));
            y = (int) (((float[][]) Objects.requireNonNull(diagramProperties.get("Text")))[i][1] *(colouredBitmap.getHeight()/2));
            canvas.drawText(String.valueOf(lettersUsed.get(i)),x, y, paint);
        }
    }

    private HashMap<String, Object> getDiagramType(ArrayList<Character> lettersUsed){
        //select which diagram needs to be drawn
        //along with the corresponding text and region positions
        HashMap<String,Object> diagramProperties = new HashMap<>();
        switch(lettersUsed.size()) {
            case 1:
                diagramProperties.put("Layout",R.drawable.one_venn_diagram);
                diagramProperties.put("Points",oneCirclePairs);
                diagramProperties.put("Text",oneCircleTextPairs);
                break;
            case 2:
                diagramProperties.put("Layout",R.drawable.two_venn_diagram);
                diagramProperties.put("Points",twoCirclePairs);
                diagramProperties.put("Text",twoCircleTextPairs);
                break;
            case 3:
                diagramProperties.put("Layout",R.drawable.three_venn_diagram);
                diagramProperties.put("Points",threeCirclePairs);
                diagramProperties.put("Text",threeCircleTextPairs);
                break;
            case 4:
                diagramProperties.put("Layout",R.drawable.four_venn_diagram);
                diagramProperties.put("Points",fourCirclePairs);
                diagramProperties.put("Text",fourCircleTextPairs);
                break;

        }
        return diagramProperties;
    }

    private boolean checkPixel(int pixelColour, int borderColour, int areaColour){
        //check whether a pixel needs its colour changing
        if(pixelColour==borderColour){
            return false;
        }
        if(pixelColour!=areaColour){
            return Color.red(pixelColour) == Color.green(pixelColour);
        }
        return false;
    }


    private Bitmap FloodFill(Bitmap diagramBitmap, float[][] points, int borderColour, String diagramFormula){
        //Use the flood fill algorithm to colour in the diagram
        Point point;
        int areaColour;
        //iterate through each region
        for(int i=0;i<points.length;i++) {
            //determine the colour for the region
            point = new Point(Math.round(diagramBitmap.getWidth() * points[i][0]), Math.round(diagramBitmap.getHeight() * points[i][1]));
            if (FormulaOperations.getFormulaValue(diagramFormula, i, variableValues, lettersUsed).equals("true")) {
                areaColour = Color.GREEN;
            } else {
                areaColour = Color.RED;
            }

            //colour the region
            Queue<Point> pointQueue = new LinkedList<>();
            pointQueue.add(point);
            while (pointQueue.size() > 0) {
                Point currentPoint = pointQueue.poll();
                if(!checkPixel(diagramBitmap.getPixel(currentPoint.x, currentPoint.y),borderColour,areaColour)){
                    continue;
                }

                Point nextPoint = new Point(currentPoint.x + 1, currentPoint.y);
                while ((currentPoint.x > 0) && (checkPixel(diagramBitmap.getPixel(currentPoint.x, currentPoint.y),borderColour,areaColour))){
                    if(diagramBitmap.getPixel(currentPoint.x, currentPoint.y)==Color.WHITE){
                        diagramBitmap.setPixel(currentPoint.x, currentPoint.y,areaColour);
                    }else {
                        //blend the area colour with the current pixel colour
                        diagramBitmap.setPixel(currentPoint.x, currentPoint.y,
                                Color.rgb((Color.red(diagramBitmap.getPixel(currentPoint.x, currentPoint.y)) + 2*Color.red(areaColour)) / 3,
                                        (Color.green(diagramBitmap.getPixel(currentPoint.x, currentPoint.y)) + 2*Color.green(areaColour)) / 3,
                                        (Color.blue(diagramBitmap.getPixel(currentPoint.x, currentPoint.y)) + 2*Color.blue(areaColour)) / 3));
                    }
                    if ((currentPoint.y > 0) && (checkPixel(diagramBitmap.getPixel(currentPoint.x, currentPoint.y - 1),borderColour,areaColour)))
                        pointQueue.add(new Point(currentPoint.x, currentPoint.y - 1));
                    if ((currentPoint.y < diagramBitmap.getHeight() - 1)
                            && (checkPixel(diagramBitmap.getPixel(currentPoint.x, currentPoint.y + 1),borderColour,areaColour)))
                        pointQueue.add(new Point(currentPoint.x, currentPoint.y + 1));
                    currentPoint.x--;
                }
                while ((nextPoint.x < diagramBitmap.getWidth() - 1)
                        && (checkPixel(diagramBitmap.getPixel(nextPoint.x, nextPoint.y),borderColour,areaColour))) {
                    if(diagramBitmap.getPixel(nextPoint.x, nextPoint.y)==Color.WHITE){
                        diagramBitmap.setPixel(nextPoint.x, nextPoint.y,areaColour);
                    }else {
                        //blend the area colour with the current pixel colour
                        diagramBitmap.setPixel(nextPoint.x, nextPoint.y,
                                Color.rgb((Color.red(diagramBitmap.getPixel(nextPoint.x, nextPoint.y)) + 2*Color.red(areaColour)) / 3,
                                        (Color.green(diagramBitmap.getPixel(nextPoint.x, nextPoint.y)) + 2*Color.green(areaColour)) / 3,
                                        (Color.blue(diagramBitmap.getPixel(nextPoint.x, nextPoint.y)) + 2*Color.blue(areaColour)) / 3));
                    }
                    if ((nextPoint.y > 0) && (checkPixel(diagramBitmap.getPixel(nextPoint.x, nextPoint.y - 1),borderColour,areaColour)))
                        pointQueue.add(new Point(nextPoint.x, nextPoint.y - 1));
                    if ((nextPoint.y < diagramBitmap.getHeight() - 1)
                            && (checkPixel(diagramBitmap.getPixel(nextPoint.x, nextPoint.y + 1),borderColour,areaColour)))
                        pointQueue.add(new Point(nextPoint.x, nextPoint.y + 1));
                    nextPoint.x++;
                }
            }
        }
        return diagramBitmap;
    }
}
