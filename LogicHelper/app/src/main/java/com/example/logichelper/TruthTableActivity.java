package com.example.logichelper;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Objects;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;


public class TruthTableActivity extends AppCompatActivity {

    private TextView bottomTextResult;
    private LinearLayout truthTableLayout;
    private String formula;
    private ArrayList<String> subformulas;
    private ArrayList<Character> lettersUsed = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.truth_table_screen);

        //setup back button
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        truthTableLayout = findViewById(R.id.truthTableLayout);
        bottomTextResult = findViewById(R.id.truthTableBottomResult);
        formula = getIntent().getStringExtra("formula");
        subformulas = getIntent().getStringArrayListExtra("subformulas");

        //order the subformulas by size
        Collections.sort(subformulas, Comparator.comparing(String::length));

        drawTable();
    }


    private HashMap<String,Integer> getDimensions(){
        //calculate the required dimensions of the table
        HashMap<String,Integer> dimensions = new HashMap<>();
        for (int i=0;i<formula.length();i++){
            Character c = formula.charAt(i);
            if((int)c>64 && (int)c<69){
                if(!lettersUsed.contains(c)){
                    lettersUsed.add(c);
                }
            }
        }
        int amountOfLetters = lettersUsed.size();
        int height = ((Double) Math.pow(2,amountOfLetters)).intValue() + 1;
        int width = amountOfLetters + subformulas.size();
        dimensions.put("height",height);
        dimensions.put("width",width);
        Collections.sort(lettersUsed);
        return dimensions;
    }


    private void drawTable(){
        HashMap<String, Integer> dimensions = getDimensions();
        int height = Objects.requireNonNull(dimensions.get("height"));
        String textToInsert;
        String formulaValue;
        String parsedFormula;

        //draw a 'table' using linear layouts
        LinearLayout letterColumns = new LinearLayout(this);
        letterColumns.setOrientation(LinearLayout.HORIZONTAL);
        HashMap<Character, ArrayList<String>> variableValues = FormulaOperations.generateVariableValues(lettersUsed);

        letterColumns.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));

        //create the variable columns
        for (int i=0;i<lettersUsed.size();i++) {
            LinearLayout rows = new LinearLayout(this);
            rows.setOrientation(LinearLayout.VERTICAL);
            TextView header = new TextView(TruthTableActivity.this);
            textToInsert = String.valueOf(lettersUsed.get(i));
            header.setText(textToInsert);
            header.setTextSize(20);
            header.setPadding(15,5,15,5);
            header.setGravity(Gravity.CENTER_HORIZONTAL);
            rows.addView(header);
            ArrayList<String> characterValues = variableValues.get(lettersUsed.get(i));
            for (int j=1;j<height;j++) {
                TextView item = new TextView(TruthTableActivity.this);
                if(characterValues!=null) {
                    textToInsert = characterValues.get(j - 1);
                }
                item.setText(textToInsert);
                item.setTextSize(20);
                item.setPadding(15,5,15,5);
                item.setGravity(Gravity.CENTER_HORIZONTAL);
                rows.addView(item);
            }
            rows.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            letterColumns.addView(rows);
        }
        truthTableLayout.addView(letterColumns);

        //create the subformula side of the table
        HorizontalScrollView scrollView = new HorizontalScrollView(this);
        scrollView.setLayoutParams(new ScrollView.LayoutParams(ScrollView.LayoutParams.WRAP_CONTENT,ScrollView.LayoutParams.WRAP_CONTENT));

        LinearLayout scrollColumns = new LinearLayout(this);
        scrollColumns.setOrientation(LinearLayout.HORIZONTAL);
        scrollColumns.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));

        int amountOfFalses = 0;
        for (int i=0;i<subformulas.size();i++) {
            LinearLayout rows = new LinearLayout(this);
            rows.setOrientation(LinearLayout.VERTICAL);
            TextView header = new TextView(TruthTableActivity.this);
            textToInsert = String.valueOf(subformulas.get(i));
            parsedFormula = FormulaParser.parseFormula(textToInsert);
            header.setText(textToInsert);
            header.setTextSize(20);
            header.setPadding(15,5,15,5);
            header.setGravity(Gravity.CENTER_HORIZONTAL);
            rows.addView(header);
            for (int j=1;j<height;j++) {
                TextView item = new TextView(TruthTableActivity.this);
                formulaValue = FormulaOperations.getFormulaValue(parsedFormula, j-1, variableValues,lettersUsed);
                if(formulaValue.equals("true")){
                    item.setText("1");
                }else{
                    item.setText("0");
                    if(i==subformulas.size()-1){
                        amountOfFalses+=1;
                    }
                }
                item.setTextSize(20);
                item.setPadding(15,5,15,5);
                item.setGravity(Gravity.CENTER_HORIZONTAL);
                rows.addView(item);
            }
            rows.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            scrollColumns.addView(rows);
        }
        scrollView.addView(scrollColumns);
        truthTableLayout.addView(scrollView);

        //determine what type of formula the largest selected formula is
        if (amountOfFalses == 0) {
            bottomTextResult.setText(R.string.tautology);
        }else if(amountOfFalses == height-1){
            bottomTextResult.setText(R.string.contradiction);
        }else{
            bottomTextResult.setText(R.string.contingency);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item){
        //finish the activity when the back button is pressed
        finish();
        return true;
    }


}
