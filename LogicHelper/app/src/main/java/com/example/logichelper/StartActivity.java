package com.example.logichelper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);

        //set intro text
        TextView introText = this.findViewById(R.id.introText);
        introText.setText(R.string.intro);

        //get button
        Button formulaButton = this.findViewById(R.id.formulaButton);

        //create listener
        formulaButtonListener formulaButtonListener = new formulaButtonListener();

        //register listener to button
        formulaButton.setOnClickListener(formulaButtonListener);
    }

    class formulaButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            //When button is clicked, get the intent object
            //Intent will start the enter formula screen activity
            Context context = v.getContext();
            Intent intent = new Intent(context, EnterFormulaActivity.class);
            startActivity(intent);
        }
    }
}
