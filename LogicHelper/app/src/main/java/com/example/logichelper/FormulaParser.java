package com.example.logichelper;

import java.util.regex.Pattern;

public class FormulaParser {

    static String parseFormula(String formulaToParse){
        StringBuilder form = new StringBuilder(formulaToParse);
        System.out.println(formulaToParse);
        boolean altered = true;
        //keep iterating through the string until we complete a run without changing anything
        while(altered) {
            altered = false;
            for (int i = 0; i < form.length(); i++) {
                //iterate through the string exchanging the inputted operators to those understood by the jexl engine
                switch (form.substring(i, i + 1)) {
                    case "\u22C0": //And
                        form.replace(i, i + 1, "&&");
                        altered = true;
                        break;
                    case "\u22C1": //Or
                        form.replace(i, i + 1, "||");
                        altered = true;
                        break;
                    case "¬": //Not
                        form.replace(i, i + 1, "!");
                        altered = true;
                        break;
                    case "\u2192": //Implies
                        form = new StringBuilder(impliesParser(form, i));
                        altered = true;
                        break;
                    case "\u2194": //Bi-implies
                        form = new StringBuilder(bi_impliesParser(form, i));
                        altered = true;
                        break;
                }
            }
        }
        return form.toString();
    }

    //change A->B to ¬AVB
    private static String impliesParser(StringBuilder formula, int index){
        int brackets=0;
        if(!(formula.charAt(index-1)==')')){
            formula.insert(index-1,'!');
        }else {
            for (int j = index - 1; j > 0; j--) {
                if (formula.charAt(j) == ')') {
                    brackets += 1;
                }
                if (formula.charAt(j) == '(') {
                    brackets -= 1;
                    if (brackets == 0) {
                        formula.insert(j, '!');
                        break;
                    }
                }

            }
        }
        formula.replace(index+1,index+2, "||");
        return formula.toString();
    }

    //change A<->B to (A/\B) \/ (¬A/\¬B)
    private static String bi_impliesParser(StringBuilder formula, int index){
        //determine the parts on the left and the right of the operator
        String preOperator = "";
        String postOperator = "";
        //left of the operator
        if(!(formula.charAt(index-1)==')')){
            preOperator = formula.substring(index-1,index);
        }else{
            int brackets=0;
            for (int j = index - 1; j > 0; j--) {
                if (formula.charAt(j) == ')') {
                    brackets += 1;
                }
                if (formula.charAt(j) == '(') {
                    brackets -= 1;
                    if (brackets == 0) {
                        preOperator = formula.substring(j,index);
                        break;
                    }
                }
            }
        }
        //right of the operator
        if(!(formula.charAt(index+1)=='(')){
            postOperator = formula.substring(index+1,index+2);
        }else {
            int brackets=0;
            for (int j = index + 1; j < formula.length(); j++) {
                if (formula.charAt(j) == '(') {
                    brackets += 1;
                }
                if (formula.charAt(j) == ')') {
                    brackets -= 1;
                    if (brackets == 0) {
                        postOperator = formula.substring(index+1,j+1);
                        break;
                    }
                }
            }
        }

        //build the new formula
        String scope = preOperator + "\u2194" + postOperator;
        String newFormula = "("+preOperator+"\u22C0"+postOperator+")"+"\u22C1"+"("+"!"+preOperator+"\u22C0"+"!"+postOperator+")";
        return formula.toString().replaceAll(Pattern.quote(scope),newFormula);
    }
}
